var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var routes = require('./server/routes/web');
var apiRoutes = require('./server/routes/api');
var connection = require('./server/config/db');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(express.static(path.join(__dirname, 'app')));
app.use(express.static('node_modules'));

app.use('/', routes);
app.use('/api', apiRoutes);

var port = 3000;

app.listen(port, function(req, res){
    console.log("Server listening at http://localhost:" + port);
});