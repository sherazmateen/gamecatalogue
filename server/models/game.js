var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var objectId = mongoose.Schema.ObjectId;

var gameSchema = new Schema({
    _id: {type: objectId, auto: true},
    name: {type: String, required: true},
    developer: {type: String, required: true}
}, {
    versionKey: false
});

var game = mongoose.model('games', gameSchema);
module.exports = game;