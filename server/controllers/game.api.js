var express = require('express');
var router = express.Router();
var game = require('../models/game');

router.get('/', function(req, res){
    game.find(function(err, data){
        if(err){
            res.send("error");
            return;
        }
        res.send(data);
    })
});

router.get('/:id', function(req, res){
    var id = req.params.id;
    game.findById(id, function(err, data){
        if(err){
            res.send("error");
            return;
        }
        res.send(data[0]);
    });
});

router.post('/', function(req, res){
    var obj = req.body;
    var model = new game(obj);

    model.save(function(err){
        if(err){
            res.send("error");
            return;
        }
        res.send("created");
    });
});

router.put('/:id', function(req, res){
    var id = req.params.id;
    var obj = req.body;

    game.findByIdAndUpdate(id, {name: obj.name, developer: obj.developer}, function(err){
        if(err){
            res.send("error");
            return;
        }
        res.send("updated");
    })
});

router.delete('/:id', function(req, res){
    var id = req.params.id;
    game.findByIdAndRemove(id, function(err){
        if(err){
            res.send("error");
            return;
        }
        res.send("deleted");
    });
});

module.exports = router;