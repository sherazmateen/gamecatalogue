var express = require('express');
var router = express.Router();
var path = require('path');

var absPath = path.join(__dirname, "../../app");

router.get('/', function(req, res){
    res.sendFile(absPath + "/index.html");
});

module.exports = router;