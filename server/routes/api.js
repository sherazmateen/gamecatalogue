var express = require('express');
var router = express.Router();

router.use('/game', require('../controllers/game.api'));

module.exports = router;